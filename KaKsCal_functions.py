def translate_seq(sequences):
	prot_sequences = {}
	for tag in sequences:
		nucl_seq = sequences[tag]
		prot_sequences[tag] = nucl_seq.translate()
	return prot_sequences

def AXT_conversion(args):
	config, rbm_file, axt_file, prot_sequences, nucl_sequences, blsTmpDir, KaKsTmpDir = args
	ifh=open(rbm_file,'r')
	ofh=open(axt_file,'w')
	nameA, nameB = re.search('.+\/(.+)\_vs\_(.+)\.rbm', rbm_file).group(1,2)	
	tmpFaa=KaKsTmpDir+nameA+'.vs.'+nameB+'.tmp.faa'
	tmpClustalW=tmpFaa.replace('faa','aln')
	tmpLog=tmpFaa.replace('faa','log')
	tmpDND=tmpFaa.replace('faa','dnd')
	
	while 1:
		line=ifh.readline().rstrip('\n')
		if not line:
			break
		col=line.split('\t')
		geneA=col[0]
		geneB=col[1]
		nuclA=nucl_sequences[geneA]
		nuclB=nucl_sequences[geneB]
		protA=prot_sequences[geneA]
		protB=prot_sequences[geneB]
		faafh=open(tmpFaa,'w')
		faafh.write('>'+geneA+'\n'+str(protA)+'\n'+'>'+geneB+'\n'+str(protB)+'\n')
		faafh.close()
			
		runClustalW2(config, tmpFaa, tmpClustalW, tmpLog)
		os.system('rm '+tmpFaa)
		alnfh=open(tmpClustalW,'r')
		alnProtA, alnProtB=SeqIO.parse(alnfh, 'clustal')
		alnNucA=reverseTranslate(nuclA, alnProtA)
		alnNucB=reverseTranslate(nuclB, alnProtB)
		ofh.write('>'+geneA+','+geneB+'\n'+str(alnNucA)+'\n'+str(alnNucB)+'\n\n')
	ofh.close()
	ifh.close()

def runKaKsCal(args):
	KaKsCalculator, axt_file, KaKs_file = args
	log = KaKs_file + '.log'
	os.system('%s -m MYN -i %s -o %s > %s' % (KaKsCalculator, axt_file, KaKs_file, log))
	os.unlink(log)
	
def calKs(config, prot_sequences, nucl_sequences):
	blsTmpDir=config.outdir+'/blsTmp/'
	KaKsTmpDir=config.outdir+'/KaKsTmp/'
	if not os.path.exists(KaKsTmpDir):
		os.mkdir(KaKsTmpDir)
	
	print 'Converting sequences into AXT files...'
	AXT_cmds = []
	for rbm in glob.glob(blsTmpDir+'/*.rbm'):
		name=re.search('.+\/(.+)\.rbm',rbm).group(1)
		axt_file=KaKsTmpDir+name+'.axt'
		if os.path.exists(axt_file): continue
		AXT_cmds.append([config, rbm, axt_file, prot_sequences, nucl_sequences, blsTmpDir, KaKsTmpDir])
	
	#for cmd in AXT_cmds: AXT_conversion(cmd)
	
	if len(AXT_cmds) > 0:
		pool = mp.Pool(int(config.nproc))
		pool.map_async(AXT_conversion, AXT_cmds)
		pool.close()
		pool.join()	
	os.system('rm ' + KaKsTmpDir+ '*.tmp.*')   # clean up
	print 'Conversion done!'
	
	print 'Calculate KaKs...'
	KaKs_cmds = []
	for axtFile in glob.glob(KaKsTmpDir+'*.axt'):
		KaKsFile=axtFile.replace('axt','KaKs')
		if os.path.exists(KaKsFile): continue
		KaKsCalculator=config.KaKsCal+ '/KaKs_Calculator'
		KaKs_cmds.append([KaKsCalculator, axtFile, KaKsFile])
	
	#for cmd in KaKs_cmds: runKaKsCal(cmd)
	
	if len(KaKs_cmds) > 0:
		pool = mp.Pool(int(config.nproc))
		pool.map_async(runKaKsCal, KaKs_cmds)
		pool.close()
		pool.join()	
		
	print 'KaKs Calculation done!'