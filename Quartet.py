#!/usr/bin/env python
# -*- coding: utf-8 -*- 

# ==============================================================================
# Quartet.py, fully automated Embedded Quartet Decomposition Analysis (EQDA) for
#				horizontal gene transfer analysis
#
# Author: Chengwei Luo (luo.chengwei@gatech.edu)
#
# Copyright: Chengwei Luo, 2013 
# Konstantinidis Laboratory, Civil and Environmental Engineering,
# Georgia Institute of Technology
#
# ==============================================================================


import sys, os, re, glob, shutil
from operator import itemgetter, attrgetter
import itertools
import networkx as nx
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO, AlignIO, Phylo
from Bio.Alphabet import generic_dna
import multiprocessing as mp
import subprocess
from subprocess import call, PIPE, Popen


class configuration:
	def __init__(self):
		self.indir=None
		self.outdir=None
		self.currentPath=None
		self.clades={}
		self.blast=None
		self.muscle=None
		self.fasttree = None
		self.PHYLIP=None
		self.KaKsCal=None
		self.puzzle=None
		self.Ks=None
		self.nproc=None
		
	def loadConfig(self, configFile):
		cfh=open(configFile)
		self.currentPath=os.path.abspath(os.getcwd())
		while 1:
			line=cfh.readline().rstrip('\n')
			if not line:
				break
			if line[0]=='#':
				continue
			col=line.split('\t')
			if col[0]=='indir':
				self.indir=col[1]
			elif col[0]=='outdir':
				self.outdir=col[1]
			elif col[0]=='blast':
				self.blast=col[1]
			elif col[0]=='muscle':
				self.muscle=col[1]
			elif col[0]=='fasttree':
				self.fasttree=col[1]
			elif col[0]=='PHYLIP':
				self.PHYLIP=col[1]
			elif col[0]=='puzzle':
				self.puzzle=col[1]
			elif col[0]=='KaKsCal':
				self.KaKsCal=col[1]
			elif col[0]=='nproc':
				self.nproc=col[1]
			elif col[0].count('clade')==1:
				cladeNum=re.search('(clade\d+)',col[0]).group(1)
				self.clades[cladeNum]=[]
				for name in col[1:]:
					if self.indir[-1]!='/':
						file=self.indir+'/'+name
					else:
						file=self.indir+name
					self.clades[cladeNum].append(file)
			else:
				sys.stderr.write("There seems something wrong with your configuration file, please double check it!\n")
				exit(1)

	def printConfig(self):
		sys.stdout.write('############### Project configurations ##############\n')
		sys.stdout.write('Current directory:%s\n' % self.currentPath)
		sys.stdout.write('Input directory:%s\n' % self.indir)
		sys.stdout.write('Output directory:%s\n' % self.outdir)
		sys.stdout.write('Project has the following genomes:\n')
		for clade in self.clades:
			sys.stdout.write('Clade'+clade+':\n')
			sys.stdout.write('%s\n' % '\n'.join(self.clades[clade]))
		sys.stdout.write('Number of threads to be used:%s\n' % self.nproc)
		sys.stdout.write('KaKsCalculator directory:\n  %s\n' % self.KaKsCal)
		sys.stdout.write('BLAST directory:\n  %s\n' % self.blast)
		sys.stdout.write('muscle directory:\n  %s\n' % self.muscle)
		sys.stdout.write('fasttree directory:\n %s\n' % self.fasttree)
		sys.stdout.write('PHYLIP directory:\n  %s\n' % self.PHYLIP)
		sys.stdout.write('PUZZLE directory:\n  %s\n' % self.puzzle)
		sys.stdout.write('######################################################\n\n')
		
class BLASTNSettings:
	def __init__(self):
		self.evalue='1e-10'
		self.perc_identity='40'
		self.outfmt='\"6 qseqid qlen sseqid slen evalue bitscore length pident\"'
		self.num_alignments='1'
		
	def settingString(self):
		option=(' -evalue '+self.evalue+' -perc_identity '+self.perc_identity+
				' -outfmt '+self.outfmt+' -num_alignments '+self.num_alignments)
		return option
		

def makeBlastDB(infile, dbName, type, fileType, blastDir, dbLog):
	command=(blastDir+'/makeblastdb '+'-in '+infile+' -input_type '+fileType
				+' -dbtype '+type+' -out '+dbName+' > '+dbLog)
	os.system(command)

def runBLASTN(args):
	db, infile, outfile, nproc, logfile, blastDir, blastConfig = args
	blastnOption=blastConfig.settingString()
	command=(blastDir+'/blastn -db '+db+' -query '+infile+' -out '+outfile
			+str(blastnOption)+' -num_threads '+ str(nproc) +' > '+logfile)
	os.system(command)
		
	
def allVSallBLAST(config):
		print '############### All-verse-all BLASTN #################'
		
		allFiles=[]
		blsTmpDir=config.outdir+'/blsTmp'
		
		if not os.path.exists(blsTmpDir):
			os.mkdir(blsTmpDir)

		for cladeName in config.clades:
			allFiles+=config.clades[cladeName]
		
		print 'Making BLASTN DBs...'
		for file in allFiles:
			name=re.search('.+\/(.+)\.\w+$', file).group(1)
			dbName=blsTmpDir+'/'+name
			dbLog=dbName+'.log'
			makeBlastDB(file, dbName, 'nucl', 'fasta', config.blast, dbLog)
		
		print 'All versus all BLASTN...'
		totalNum=len(allFiles)*(len(allFiles)-1)
	
		blast_cmds = []
		for indexA in range(len(allFiles)):
			fileA=allFiles[indexA]
			nameA=re.search('.+\/(.+)\.\w+$', fileA).group(1)
			dbName=blsTmpDir+'/'+nameA
			for indexB in range(len(allFiles)):
				if indexA==indexB:
					continue
				fileB=allFiles[indexB]
				nameB=re.search('.+\/(.+)\.\w+$', fileB).group(1)
				outfile=blsTmpDir+'/'+nameB+'_vs_'+nameA+'.bls'
				if os.path.exists(outfile): continue
				logfile=outfile.replace('bls$','log')
				#blsIndex+=1
				#print 'Now running BLASTN on '+nameA+' versus '+nameB+'\t['+str(blsIndex)+'/'+str(totalNum)+' finished]\r'
				blastConfig=BLASTNSettings()
				blast_cmds.append([dbName, fileB, outfile, config.nproc, logfile, config.blast, blastConfig])
		
		pool = mp.Pool(int(config.nproc))
		pool.map_async(runBLASTN, blast_cmds)
		pool.close()
		pool.join()
		#runBLASTN(dbName, fileB, outfile, config.nproc, logfile, config.blast, blastConfig)
		
		print 'All done!'
		print '#######################################################'
		
		

def RBMExtract(config):
	blsTmpDir=config.outdir+'/blsTmp/'
	
	allFiles=[]
	for cladeName in config.clades:
		allFiles+=config.clades[cladeName]
	
	for indexA in range(len(allFiles)):
		fileA=allFiles[indexA]
		nameA=re.search('.+\/(.+)\.\w+$', fileA).group(1)
		dbName=blsTmpDir+'/'+nameA
		for indexB in range(len(allFiles)):
			if indexA>=indexB:
				continue
			fileB=allFiles[indexB]
			nameB=re.search('.+\/(.+)\.\w+$', fileB).group(1)
			blsFileA=blsTmpDir+'/'+nameB+'_vs_'+nameA+'.bls'
			blsFileB=blsTmpDir+'/'+nameA+'_vs_'+nameB+'.bls'
			outfile=blsTmpDir+'/'+nameA+'_vs_'+nameB+'.rbm'
			ifhA=open(blsFileA,'r')
			ifhB=open(blsFileB,'r')
			ofh=open(outfile,'w')
			
			libA={}
			libB={}
			while 1:
				line=ifhA.readline().rstrip('\n')
				if not line:
					break
				col=line.split('\t')
				query=col[0]
				qlen=int(col[1])
				subject=col[2]
				slen=int(col[3])
				pident=float(col[-1])
				alignlen=int(col[-2])
				palign=float(alignlen)/max(qlen, slen)
				if qlen<300 or slen<300:
					continue
				if palign < 0.85:
					continue
				if query in libA:
					continue
				libA[query]=(subject, pident)
			ifhA.close()
			
			while 1:
				line=ifhB.readline().rstrip('\n')
				if not line:
					break
				col=line.split('\t')
				query=col[0]
				qlen=int(col[1])
				subject=col[2]
				slen=int(col[3])
				pident=float(col[-1])
				alignlen=int(col[-2])
				palign=float(alignlen)/max(qlen, slen)
				if qlen<300 or slen<300:
					continue
				if palign < 0.85:
					continue
				if query in libB:
					continue
				libB[query]=(subject, pident)
			ifhB.close()
			
			for geneA in libA:
				geneB=libA[geneA][0]
				pidentA=libA[geneA][1]
				if geneB in libB and libB[geneB][0]==geneA:
					pidentB=libB[geneB][1]
					pident=(pidentA+pidentB)/2
					ofh.write(geneA+'\t'+geneB+'\t'+str(pident)+'\n')
			ofh.close()
	print 'RBM extraction done!'	

def constructPan(config):
	print 'Retangling Graph...'
	G=nx.Graph()
	blsTmpDir=config.outdir+'/blsTmp/'
	
	allFiles=[]
	for cladeName in config.clades:
		allFiles+=config.clades[cladeName]
	
	## add all the genes into Graph
	Genomes=[]
	totalGeneNum=0
	for file in allFiles:
		allGenes=[]
		genome=re.search('.+\/(.+)\.\w+$',file).group(1)
		Genomes.append(genome)
		for record in SeqIO.parse(file, 'fasta'):
			allGenes.append(record.description)
		for gene in allGenes: G.add_node(gene, origin=genome)
		totalGeneNum+=len(allGenes)
	print str(totalGeneNum)+' nodes added to Graph!'
	
	## add all rbm relationships into Graph
	allRBMs=[]
	for RBMFile in glob.glob(blsTmpDir+'*.rbm'):
		ifh=open(RBMFile,'r')
		while 1:
			line=ifh.readline().rstrip('\n')
			if not line:
				break
			col=line.split('\t')
			edge=(col[0], col[1])
			allRBMs.append(edge)
		ifh.close()
	G.add_edges_from(allRBMs)
	print str(len(allRBMs))+' RBM relationships added to Graph!'
	
	## retrieve all the orthologs, and write to pangenome.txt
	print 'Now retrieving orthologs...'
	pangenomeFile=config.outdir+'/pangenome.txt'
	ofh=open(pangenomeFile,'w')
	ofh.write('probe\t'+'\t'.join(Genomes)+'\n')
	
	geneIndex=0
	for component in nx.connected_components(G):
		geneIndex+=1
		genes={}
		numGenes=len(component)
		for comp in component:
			origin=G.node[comp]['origin']
			if origin not in genes:
				genes[origin]=[]
			genes[origin].append(comp)
			
		sortedGenes=[]
		for genome in Genomes:
			if genome not in genes:
				sortedGenes.append('-')
			else:
				sortedGenes.append(','.join(genes[genome]))
		ofh.write('PAN'+str(geneIndex)+'('+str(numGenes)+')'+'\t'+'\t'.join(sortedGenes)+'\n')
	ofh.close()
	print 'Pangenome construction done! Results are saved at '+pangenomeFile
	return geneIndex

def constructPangenome(config):
	print 'Now constructing pangenomes...'
	allVSallBLAST(config)
	RBMExtract(config)
	pangenomeSize=constructPan(config)
	shutil.rmtree(config.outdir+'/blsTmp')
	print 'Pangenome done, it has '+str(pangenomeSize)+' orthologs'
	
def loadAllSequences(config):
	print 'Loading all the nucleotide sequences...'
	sequences={}
	allFiles=[]
	for cladeName in config.clades:
		allFiles+=config.clades[cladeName]	
	for file in allFiles:
		for record in SeqIO.parse(file, 'fasta'):
			sequences[record.description] = record.seq
	print 'Loading done!'
	return sequences
	
def reverseTranslate(nucl, aln):
	alnNuc=''
	for index in range(len(aln)):
		if str(aln[index])!='-':
			alnNuc+=nucl[3*index:3*index+3]
		else:
			alnNuc+='---'
	return alnNuc

def align_pangenome(config, all_nucl):
	pangenomeFile=config.outdir+'/pangenome.txt'
	ffn_dir = config.outdir+'/ffnTmp'
	aln_dir = config.outdir+'/alnTmp'
	if not os.path.exists(aln_dir): os.mkdir(aln_dir)
	if not os.path.exists(ffn_dir): os.mkdir(ffn_dir)
	pfh = open(pangenomeFile, 'r')
	header = pfh.readline().rstrip('\n').split('\t')
	sequences = {}
	while 1:
		line = pfh.readline().rstrip('\n')
		if not line: break
		cols = line.split('\t')
		pan_id = re.search('(PAN\d+)', cols[0]).group(1)
		aln_file = aln_dir + '/' + pan_id + '.aln'
		if os.path.exists(aln_file): continue
		sequences[pan_id] = []
		for index, x in enumerate(cols[1:], 1):
			if x == '-': continue
			tag = x.split(',')[0]
			genome = header[index]
			try:seq = all_nucl[tag]
			except: continue
			sequences[pan_id].append((genome, tag, seq))
	pfh.close()
	
	# make sequences
	cmds = []
	# get genome to clade dict:
	clade_dict = {}
	for clade in config.clades:
		for genome_file in config.clades[clade]:
			genome = os.path.basename(genome_file).replace('.ffn', '')
			clade_dict[genome] = clade
			
	for pan_id in sequences:
		all_genomes = map(itemgetter(0), sequences[pan_id])
		all_clades = [clade_dict[a] for a in all_genomes]
		if len(set(all_clades)) < 2: continue
		seq_file = ffn_dir + '/' + pan_id + '.ffn'
		sfh = open(seq_file, 'w')
		for genome, tag, seq in sequences[pan_id]: sfh.write('>%s\n%s\n' % (genome, seq))
		sfh.close()
		aln_file = aln_dir + '/' + pan_id + '.aln'
		cmds.append([config, seq_file, aln_file])
	
	pool = mp.Pool(int(config.nproc))
	pool.map_async(aln_ortholog, cmds)
	pool.close()
	pool.join()
	
def aln_ortholog(args):
	config, infile, outfile = args
	call([config.muscle, '-in', infile, '-out', outfile], stderr = PIPE, stdout = PIPE)

def makeCoreGenomePhylogeny(config):
	print 'Concatenating alignments...'
	# select genes
	pangenome=config.outdir+'pangenome.txt'
	pfh=open(pangenome,'r')
	header = pfh.readline().rstrip('\n').split('\t')
	core_genes = {}
	while 1:
		line = pfh.readline().rstrip('\n')
		if not line: break
		cols = line.split('\t')
		pan_id, pan_size_st = re.search('(PAN\d+)\((\d+)\)', cols[0]).group(1,2)
		pan_size = int(pan_size_st)
		if pan_size == len(cols[1:]): core_genes[pan_id] = 1
	pfh.close()
	
	alnTmpDir=config.outdir+'/alnTmp/'
	coreAlnFile=config.outdir+'coreGenome.aln'
	coreTreeFile=coreAlnFile.replace('.aln','.ph')
	core_seqs = {}
	for pan_id in core_genes:
		aln_file = alnTmpDir+pan_id+'.aln'
		seqs = {}
		for record in SeqIO.parse(aln_file, 'fasta'):
			genome = record.name
			seq = record.seq
			seqs[genome] = seq
		if len(seqs) < len(header) - 1: continue
		for genome in seqs:
			if genome not in core_seqs: core_seqs[genome] = ''
			core_seqs[genome] += seqs[genome]
			
	# write the core aligned genome
	cgfh=open(coreAlnFile,'w')
	for genome in core_seqs: cgfh.write('>%s\n%s\n' % (genome, core_seqs[genome]))
	cgfh.close()
	cmd = '%s -nt < %s > %s' % (config.fasttree, coreAlnFile, coreTreeFile)
	os.system(cmd)
	print 'Tree construction done, the results are saved at:'
	print coreTreeFile

def getAlpha(puzzleFile):
	pfh=open(puzzleFile,'r')
	alpha=0
	while 1:
		line=pfh.readline()
		if not line:
			break
		if re.match('Gamma distribution parameter',line):
			ele=line.split(' ')
			alpha=float(ele[8])
	pfh.close()
	return alpha

def fileLines(infile):
    p = subprocess.Popen(['wc', '-l', infile], 
    				stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])

def quartetDecomp(config):
	print "Now starting Embedded Quartet Decomposition Analysis (EQDA) ..."
	
	alnTmpDir=config.outdir+'/alnTmp/'
	coreTreeFile=config.outdir+'coreGenome.ph'
	
	# create cmds files
	logfile=config.outdir+'tmp.log'
	
	puzzleCmds=config.outdir+'puzzle.cmds'
	pfh=open(puzzleCmds,'w')
	pfh.write('k\nk\nk\nw\nc\n4\nm\nm\ny\n')
	pfh.close()
	
	# create directories
	seqbootTmpDir=config.outdir+'/seqbootTmp/'
	if not os.path.exists(seqbootTmpDir): os.mkdir(seqbootTmpDir)
	distTmpDir=config.outdir+'/distTmp/'
	if not os.path.exists(distTmpDir): os.mkdir(distTmpDir)
	phybootTmpDir=config.outdir+'/phybootTmp/'
	if not os.path.exists(phybootTmpDir): os.mkdir(phybootTmpDir)
	
	# convert from fasta .aln files to .phy files
	print "Converting .aln files to .phy files"
	for aln in glob.glob(alnTmpDir+'*.aln'):
		phy_file = aln.replace('.aln', '.phy')
		if os.path.exists(phy_file): continue
		alignments = AlignIO.parse(open(aln, 'rU'), 'fasta')
		AlignIO.write(alignments, open(phy_file, 'w'), 'phylip')
	print "Done."
	
	## get alphas
	alphaFile=config.outdir+'alpha.txt'
	alpha_cmds = []
	if not os.path.exists(alphaFile):
		alphafh=open(alphaFile,'w')
	
		for phyFile in glob.glob(alnTmpDir+'*.phy'):
			panID=re.search('(PAN\d+)\.phy', phyFile).group(1)
			# puzzle estimate alpha	
			command=config.puzzle+'puzzle '+phyFile+' < '+puzzleCmds+' > '+logfile
			os.system(command)
			# get the alpha shape parameter
			distFile=phyFile+'.dist'
			puzzleFile=phyFile+'.puzzle'
			try:os.remove(distFile)
			except:continue
			alpha=getAlpha(puzzleFile)
			alphafh.write(panID+'\t'+str(alpha)+'\n')
			os.remove(puzzleFile)
		alphafh.close()
		print "Gamma distribution parameter alpha estimation done!"
	
	## seqboot 100 bootstrap trees
	print "Starting bootstrapping..."
	for phyFile in glob.glob(alnTmpDir+'*.phy'):
		panID=re.search('(PAN\d+)\.phy', phyFile).group(1)
		seqbootFile=seqbootTmpDir+panID+'.seqboot'
		if os.path.exists(seqbootFile): continue
		seqbootCmds=config.outdir+'/seqboot.cmds'
		sfh=open(seqbootCmds,'w')
		sfh.write(phyFile+'\ny\n17\n')
		sfh.close()
		seqbootOutfile=config.currentPath+'/outfile'
		command=config.PHYLIP+'seqboot < '+seqbootCmds+' > '+logfile
		os.system(command)
		shutil.move(seqbootOutfile, seqbootFile)
	print "Bootstrapping done!"
	
	## get the dist
	alphafh=open(alphaFile,'r')
	alphas={}
	while 1:
		line=alphafh.readline()
		if not line:
			break
		col=line.rstrip('\n').split('\t')
		panID=col[0]
		alpha=col[1]
		alphas[panID]=float(alpha)
	alphafh.close()
	
	for seqbootFile in glob.glob(seqbootTmpDir+'*.seqboot'):
		panID=re.search('(PAN\d+)\.seqboot', seqbootFile).group(1)
		try:alpha=alphas[panID]
		except KeyError: continue
		genDistFile=distTmpDir+panID+'.dist'
		numLines=fileLines(seqbootFile)
		perFileLines=numLines/100
		command='split -l '+str(perFileLines)+' '+seqbootFile+' '+seqbootFile+'.'
		os.system(command)
		
		puzzleCmds=config.outdir+'puzzle.cmds'
		pfh=open(puzzleCmds,'w')
		pfh.write('k\nk\nk\nw\nc\n4\na\n'+str(alpha)+'\nm\nm\ny\n')
		pfh.close()
		
		for subSeqbootFile in glob.glob(seqbootFile+'.*'):
			command=config.puzzle+'puzzle '+subSeqbootFile+' < '+puzzleCmds+' > '+logfile
			os.system(command)
			distFile=subSeqbootFile+'.dist'
			puzzleFile=subSeqbootFile+'.puzzle'
			command='cat '+distFile+' >> '+genDistFile
			os.system(command)
			try:
				os.remove(distFile)
				os.remove(subSeqbootFile)
				os.remove(puzzleFile)
			except OSError:
				continue
	
	## build N-J trees
	for distFile in glob.glob(distTmpDir+'*.dist'):
		panID=re.search('(PAN\d+)\.dist',distFile).group(1)
		phybootFile=phybootTmpDir+panID+'.phyboot'
		neighborCmds=config.outdir+'neighbor.cmds'
		nfh=open(neighborCmds,'w')
		nfh.write(distFile+'\nm\n100\n17\ny\n')
		nfh.close()
		command=config.PHYLIP+'neighbor < '+neighborCmds+' > '+logfile
		os.system(command)
		neighborOutfile=config.currentPath+'/outfile'
		neighborOuttree=config.currentPath+'/outtree'
		os.remove(neighborOutfile)
		phybootFile=phybootTmpDir+panID+'.phyboot'
		shutil.move(neighborOuttree, phybootFile)
	print "All distances calculation and tree building done!"
	
def genAllQuartets(elements, cladeDict):
	if len(elements)<4:
		return []
	preCombinations=list(itertools.combinations(elements, 4))
	Combinations=[]
	for comb in preCombinations:
		clades={}
		for ele in comb:
			cladeNum=cladeDict[ele]
			if cladeNum not in clades:
				clades[cladeNum]=1
			else:
				clades[cladeNum]+=1
		if len(clades.keys())!=2:
			continue
		if clades.values()[0]==2 and clades.values()[1]==2:
			Combinations.append(comb)
	return Combinations
				
def avgKsCal(lib, panID):
	avgKs=0
	avgKsSite=0
	if panID not in lib:
		return avgKs, avgKsSite
	
	numPairs=len(lib[panID])
	totalKs=0
	totalKsSite=0
	for pair in lib[panID]:
		totalKs+=pair[0]
		totalKsSite+=pair[1]
	avgKs=totalKs/numPairs
	avgKsSite=totalKsSite/numPairs
	
	return avgKs, avgKsSite
				
def quartetGraph(config):
	print 'Loading basic information...'
	## load core genome phylogeny as reference
	coreGenomeTree=config.outdir+'coreGenome.ph'
	genomeTree=list(Phylo.parse(coreGenomeTree, 'newick'))[0]
	
	# store the pan id that need to be analyzed
	
	
	
	qualified_panids = {}
	for dist_file in glob.glob(dist_dir+'*.dist'):
		pan_id = os.path.basename(dist_file).replace('.dist','')
		qualified_panids[pan_id] = 1
	# get genome to clade dict:
	clade_dict = {}
	for clade in config.clades:
		for genome_file in config.clades[clade]:
			genome = os.path.basename(genome_file).replace('.ffn', '')
			clade_dict[genome] = clade
	
	gene_pan_dict={}
	pan_dict = {}
	pangenome_file=config.outdir+'pangenome.txt'
	pfh=open(pangenome_file,'r')
	header=pfh.readline().rstrip('\n').split('\t')
	numGenomes = len(header)-1
	pangenome={}
	while 1:
		line=pfh.readline().rstrip('\t')
		if not line: break
		cols = line.split('\t')
		panID=re.search('(PAN\d+)', cols[0]).group(1)
		if panID not in qualified_panids: continue
		genes = []
		for index, x in enumerate(cols[1:],1):
			if x == '-': continue
			gene = x.split(',')[0]
			genome = header[index]
			clade = clade_dict[genome]
			pangenome[panID] = [gene, genome, clade]
	pfh.close()
	
	cladeDict={}
	for cladeNum in config.clades:
		genomeFiles=config.clades[cladeNum]
		for genomeFile in genomeFiles:
			genome=re.search('.+\/(.+)\.\w+$',genomeFile).group(1)
			cladeDict[genome]=cladeNum
	
	## go through all qualified orthologs and compare quartet topologies
	print 'Quartet analysis, without Ka/Ks filtering...\n'
	outfile=config.outdir+'/HGT_candidates.txt'
	ofh=open(outfile,'w')
	for panID in qualified_panids:
		phybootFile = phybootTmpDir+panID+'.phyboot'
		distFile = dist_dir+panID+'.dist'
		# see if core gene or not
		gene_category = 'N'
		if len(pangenome[panID]) == numGenomes: gene_category = 'C'
		
		trees=list(Phylo.parse(phybootFile, 'newick'))
		leaves=[]
		for leaf in trees[0].get_terminals():
			leaves.append(leaf.name)

		try: allQuartetList=genAllQuartets(leaves, cladeDict)
		except: continue
		for nodes in allQuartetList:
			nodeA, nodeB, nodeC, nodeD = nodes
			nodeClades = [cladeDict[nodeA], cladeDict[nodeB], cladeDict[nodeC], cladeDict[nodeD]]
			sortedNodes = sorted(zip(nodeClades, nodes), key = lambda x: x[0])
			nodeA = sortedNodes[0][1]
			nodeB = sortedNodes[1][1]
			nodeC = sortedNodes[2][1]
			nodeD = sortedNodes[3][1]
			clade1 = sortedNodes[0][0]
			clade2 = sortedNodes[2][0]
			topo1 = 0
			topo2 = 0
			topo3 = 0
			for tree in trees:
				try:
					distAB = tree.distance(nodeA, nodeB)
					distCD = tree.distance(nodeC, nodeD)
					distAC = tree.distance(nodeA, nodeC)
					distAD = tree.distance(nodeA, nodeD)
					distBC = tree.distance(nodeB, nodeC)
					distBD = tree.distance(nodeB, nodeD)
					cross_dists = [distAC, distAD, distBC, distBD]
					if distAB < min(cross_dists) and distCD < min(cross_dists):
						topo1 += 1
					if distAB > min(cross_dists):
						if distAB > distAC or distAB > distBC: topo2 += 1
						elif distAB > distAD or distAB > distBD: topo3 += 1
					if distCD > min(cross_dists):
						if distCD > distAC or distCD > distAD: topo3 += 1
						elif distCD > distBC or distCD > distBD: topo2 += 1
				except:
					continue
			S = sum([topo1, topo2, topo3])
			if S < 50: continue
			r1 = float(topo2)/S
			r2 = float(topo3)/S
			if r1 > 0.95:
				rt1 = '%.4f' % r1
				ofh.write(nodeA+'\t'+nodeC+'\t'+panID+'\t'+gene_category+'\t'+str(S)+'\t'+str(rt1)+'\n')
				ofh.write(nodeB+'\t'+nodeD+'\t'+panID+'\t'+gene_category+'\t'+str(S)+'\t'+str(rt1)+'\n')
			if r2 > 0.95:
				rt2 = '%.4f' % r2
				ofh.write(nodeA+'\t'+nodeD+'\t'+panID+'\t'+gene_category+'\t'+str(S)+'\t'+str(rt2)+'\n')
				ofh.write(nodeB+'\t'+nodeC+'\t'+panID+'\t'+gene_category+'\t'+str(S)+'\t'+str(rt2)+'\n')
	ofh.close()
	
	print 'Now adding Ka/Ks resutls...\n'
	
def main():
	#read the project configuration file
	configFile=sys.argv[1]
	config=configuration()
	config.loadConfig(configFile)
	config.printConfig()
	sys.stdout.write('Configurations loading done!\n')
	if not os.path.exists(config.outdir): os.mkdir(config.outdir)
	
	# construct the pangenome
	nucl_sequences=loadAllSequences(config) # load all sequences into RAM
	if not os.path.exists(config.outdir + '/progress.log'):
		with open(config.outdir+'progress.log', 'w') as ofh: ofh.write('0\n')
	with open(config.outdir+'progress.log', 'r') as cfh: 
		progress_code = int(cfh.readline().rstrip('\n'))
	if progress_code < 1: 
		constructPangenome(config)
		with open(config.outdir+'progress.log', 'w') as ofh: ofh.write('1\n')
		sys.stdout.write('#######################################################\n')
		
	with open(config.outdir+'progress.log', 'r') as cfh: 
		progress_code = int(cfh.readline().rstrip('\n'))
	if progress_code < 2:
		# align all orthologs nucl using muscle
		sys.stdout.write('Aligning nucleotide sequences using muscle...\n')
		align_pangenome(config, nucl_sequences)
		sys.stdout.write('Done.\n')
		
		coreGenomeTree = config.outdir+'/coreGenome.ph'
		if not os.path.exists(coreGenomeTree):
			sys.stdout.write('Constructing genome phylogenetic tree...\n')
			makeCoreGenomePhylogeny(config)
			sys.stdout.write('Done.\n')
		
		with open(config.outdir+'progress.log', 'w') as ofh: ofh.write('2\n')
		sys.stdout.write('#######################################################\n\n')
	
	with open(config.outdir+'progress.log', 'r') as cfh: 
		progress_code = int(cfh.readline().rstrip('\n'))
	if progress_code < 3:
		sys.stdout.write('Constructing ortholog distances and bootstrapping trees...\n')
		quartetDecomp(config)
		with open(config.outdir+'progress.log', 'w') as ofh: ofh.write('3\n')
		sys.stdout.write('#######################################################\n\n')

	with open(config.outdir+'progress.log', 'r') as cfh: 
		progress_code = int(cfh.readline().rstrip('\n'))
	if progress_code < 4:
		sys.stdout.write('Testing all possible HGTs events...\n')
		quartetGraph(config)
		with open(config.outdir+'progress.log', 'w') as ofh: ofh.write('4\n')
		sys.stdout.write('#######################################################\n\n')
	exit()
	with open(config.outdir+'progress.log', 'r') as cfh: 
		progress_code = int(cfh.readline().rstrip('\n'))
	if progress_code < 5:
		sys.stdout.write('Add Ka/Ks information...\n')
		with open(config.outdir+'progress.log', 'w') as ofh: ofh.write('5\n')
		sys.stdout.write('#######################################################\n\n')	
	sys.stdout.write('All done, bye!\n\n')
	
if __name__=='__main__':
	main()
